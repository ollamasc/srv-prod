package com.techuniversity.prod.controllers;

import com.techuniversity.prod.servicios.ServiciosService;
import org.json.*;
import org.bson.Document;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/servicios")
public class ServicioController {

    /*@GetMapping("/servicios")
    public List getServicios(){
        return ServiciosService.getAll();
    } */

    @PostMapping("/servicio")
    public String setServicio(@RequestBody String cadena){
        try{
            ServiciosService.insert(cadena);
            return "OK";
        }
        catch (Exception ex){
            return ex.getMessage();
        }
    }
    @PostMapping("/servicios")
    public String setServicios(@RequestBody String cadena){
        try{
            ServiciosService.insertBatch(cadena);
            return "OK";
        }
        catch (Exception ex){
            return ex.getMessage();
        }
    }
    @GetMapping("/servicios")
    public List getServicios(@RequestBody String filtro){
        return ServiciosService.getFiltrados(filtro);
    }

    @GetMapping("/servicios/periodo")
    public List getServiciosPeriodo(@RequestParam String periodo){
        Document doc = new Document();
        doc.append("disponibilidad.periodos", periodo);
        return ServiciosService.getFiltradosPeriodo(doc);
    }

    @PutMapping("/servicios")
    public String updServicios(@RequestBody String data){
        try{

            JSONObject obj = new JSONObject(data);
            String filtro = obj.getJSONObject("filtro").toString();
            String valores = obj.getJSONObject("valores").toString();
            ServiciosService.update(filtro,valores);
            return "OK";

        } catch (Exception ex){
           return ex.getMessage();
        }

    }
}
